import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Amplify from 'aws-amplify';
import awsconfig from './aws-exports';
import { withAuthenticator } from 'aws-amplify-react-native'; // or 'aws-amplify-react-native';
import { ThemeProvider, Header, SearchBar, Avatar, ButtonGroup } from 'react-native-elements';
import { MapView } from 'expo';
import Icon from 'react-native-vector-icons/FontAwesome';

// import AnimatedMarkers from './containers/simpleMap';
// Amplify.configure(awsconfig);
const latV = 37;
const longV = -122;
var colorArray = ['#4f74af', '#8d2bce', '#0f8421', '#84360f', '#0dadbf'];
const markerArray = new Array(100).fill(0).map((v, i) => {
  return {
    title: Math.ceil(Math.random() * 500) + ' likes',
    latitude: latV + Math.random(),
    longitude: longV - Math.random(),
    pinColor: colorArray[Math.floor(Math.random() * 5)]
  }
});
const searchBarProps = {
  searchBarStyles: {
    backgroundColor: '#fff',
    marginTop: 8,
  },
  source: {
    uri: 'https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjV8ODvnO3iAhVDQN4KHVIWDzUQjRx6BAgBEAU&url=https%3A%2F%2Fwww.istockphoto.com%2Fphotos%2Fsmiling-woman&psig=AOvVaw21xWK8L2cP5oOrzGFd8DLD&ust=1560747821262196'
  }
}

function SearchComponent() {
  const [search, updateSearch] = useState('');

  return (
    <SearchBar
      containerStyle={searchBarProps.searchBarStyles}
      inputContainerStyle={searchBarProps.searchBarStyles}
      inputStyle={searchBarProps.searchBarStyles}
      placeholder="Type Here..."
      onChangeText={(val) => updateSearch(val)}
      value={search}
    />
  )
}

const button1 = () => <Icon name="plus" />
const button2 = () => <Icon name="filter" />
const button3 = () => <Icon name="user" />

const buttons = [
  { element: button1 },
  { element: button2 },
  { element: button3 },
]

function ButtonGroupElement() {
  [selectIndex, setSelectIndex] = useState(1);

  return (
    <ButtonGroup
      buttonStyle={{ height: 5, backgroundColor: '#f4c542' }}
      containerStyle={{ backgroundColor: '#f4c542' }}
      onPress={index => this.setSelectIndex(index)}
      selectedIndex={selectIndex}
      buttons={buttons}
      containerStyle={{ height: 100 }}
    />
  )
}


export default function App() {
  return (
    <ThemeProvider>
      <SearchComponent></SearchComponent>
      <MapView
        style={{ flex: 1 }}
        initialRegion={{
          latitude: 37.78825,
          longitude: -122.4324,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}>
        {
          markerArray.map(v => {
            return <MapView.Marker coordinate={{ latitude: v.latitude, longitude: v.longitude }} title={v.title} pinColor={v.pinColor}>
              {/* <View style={{ flexDirection: 'row' }}>
                <Text>{Math.ceil(Math.random() * 100)}</Text><Icon name="heart" style={{ color: '#f45642' }} />
              </View> */}
            </MapView.Marker>
          })
        }
      </MapView>
      <ButtonGroupElement></ButtonGroupElement>
    </ThemeProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

// export default withAuthenticator(App, true);